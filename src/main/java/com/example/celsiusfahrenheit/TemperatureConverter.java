package com.example.celsiusfahrenheit;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;


public class TemperatureConverter extends HttpServlet {
    private Converter converter;

    public void init() throws ServletException {
        converter = new Converter();
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
        throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Temperature Converter";
        out.println(docType + "<HTML>\n" + "<HEAD><TITLE>" + title +
                "</TITLE>" + "</HEAD\n" + "<H1>" + title + "</H1>\n" +
                "<P> Temperature in Celsius: " + request.getParameter("temperature")
                + "\n" + "<P> Temperature in Fahrenheit: " + (converter.getFahrenheitTemp(Double.parseDouble(request.getParameter("temperature"))))
                + "</BODY></HTML>");

    }

    public void destroy() {

    }
}