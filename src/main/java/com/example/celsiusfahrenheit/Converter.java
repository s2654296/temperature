package com.example.celsiusfahrenheit;

public class Converter {
    public double getFahrenheitTemp (Double celsiusTemp) {
        return (celsiusTemp * 9)/5 + 32;
    }
}
